CFLAGS := $(shell pkg-config --cflags gnutls)
LIBS := $(shell pkg-config --libs gnutls)
OBJS := ex-client-x509.o tcp.o verify.o
PROGRAM := ex-client-x509

.PHONY: all
all: $(PROGRAM)

%.o: %.c
	gcc $(CFLAGS) -o $@ -c $<

$(PROGRAM): $(OBJS)
	gcc -o $@ $^ $(LIBS)

.PHONY: clean
clean:
	rm -f $(PROGRAM) $(OBJS)

