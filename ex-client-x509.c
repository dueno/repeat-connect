/* This example code is placed in the public domain. */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include "examples.h"

/* A very basic TLS client, with X.509 authentication and server certificate
 * verification. Note that error recovery is minimal for simplicity.
 */

#define CHECK(x) assert((x) >= 0)
#define LOOP_CHECK(rval, cmd)                                             \
	do {                                                              \
		rval = cmd;                                               \
	} while (rval == GNUTLS_E_AGAIN || rval == GNUTLS_E_INTERRUPTED); \
	assert(rval >= 0)

#define MAX_BUF 1024
#define MSG "GET / HTTP/1.0\r\n\r\n"

extern int tcp_connect(const char *host, const char *port);
extern void tcp_close(int sd);

static void
try_connect(gnutls_certificate_credentials_t xcred,
	    const char *host, const char *port)
{
	int ret, sd, ii;
	gnutls_session_t session;
	char buffer[MAX_BUF + 1], *desc;
	gnutls_datum_t out;
	int type;
	unsigned status;

	/* If client holds a certificate it can be set using the following:
	 *
	 gnutls_certificate_set_x509_key_file (xcred, "cert.pem", "key.pem", 
	 GNUTLS_X509_FMT_PEM); 
	 */

	/* Initialize TLS session */
	CHECK(gnutls_init(&session, GNUTLS_CLIENT));

	CHECK(gnutls_server_name_set(session, GNUTLS_NAME_DNS,
				     host,
				     strlen(host)));

	/* It is recommended to use the default priorities */
	CHECK(gnutls_set_default_priority(session));

	/* put the x509 credentials to the current session
	 */
	CHECK(gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred));
	gnutls_session_set_verify_cert(session, host, 0);

	/* connect to the peer
	 */
	sd = tcp_connect(host, port);

	gnutls_transport_set_int(session, sd);
	gnutls_handshake_set_timeout(session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

	/* Perform the TLS handshake
	 */
	do {
		ret = gnutls_handshake(session);
	} while (ret < 0 && gnutls_error_is_fatal(ret) == 0);
	if (ret < 0) {
		if (ret == GNUTLS_E_CERTIFICATE_VERIFICATION_ERROR) {
			/* check certificate verification status */
			type = gnutls_certificate_type_get(session);
			status = gnutls_session_get_verify_cert_status(session);
			CHECK(gnutls_certificate_verification_status_print(
				status, type, &out, 0));
			printf("cert verify output: %s\n", out.data);
			gnutls_free(out.data);
		}
		fprintf(stderr, "*** Handshake failed: %s\n",
			gnutls_strerror(ret));
		goto end;
	} else {
		desc = gnutls_session_get_desc(session);
		printf("- Session info: %s\n", desc);
		gnutls_free(desc);
	}

	/* send data */
	LOOP_CHECK(ret, gnutls_record_send(session, MSG, strlen(MSG)));

	LOOP_CHECK(ret, gnutls_record_recv(session, buffer, MAX_BUF));
	if (ret == 0) {
		printf("- Peer has closed the TLS connection\n");
		goto end;
	} else if (ret < 0 && gnutls_error_is_fatal(ret) == 0) {
		fprintf(stderr, "*** Warning: %s\n", gnutls_strerror(ret));
	} else if (ret < 0) {
		fprintf(stderr, "*** Error: %s\n", gnutls_strerror(ret));
		goto end;
	}

	if (ret > 0) {
		printf("- Received %d bytes: ", ret);
		for (ii = 0; ii < ret; ii++) {
			fputc(buffer[ii], stdout);
		}
		fputs("\n", stdout);
	}

	CHECK(gnutls_bye(session, GNUTLS_SHUT_RDWR));

end:

	tcp_close(sd);

	gnutls_deinit(session);
}

int main(int argc, char **argv)
{
	const char *host, *port;
	long interval;
	gnutls_certificate_credentials_t xcred;

	assert(argc == 4);

	host = argv[1];
	port = argv[2];
	interval = strtol(argv[3], NULL, 0);
	assert(interval > 0);

	/* X509 stuff */
	CHECK(gnutls_certificate_allocate_credentials(&xcred));

	/* sets the system trusted CAs for Internet PKI */
	CHECK(gnutls_certificate_set_x509_system_trust(xcred));

	while (true) {
		try_connect(xcred, host, port);
		sleep(interval);
	}

	gnutls_certificate_free_credentials(xcred);

	return 0;
}
